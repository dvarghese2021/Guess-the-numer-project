package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        boolean play = false;
        String repeat = "";
        do {
            String name = "";
            int num = 0;
            int count = 0;

            System.out.println("\n" + "Hello! What is your name?");
            Scanner input = new Scanner(System.in);

            name = input.nextLine();

            System.out.println("\033[0;1m" + name);
            System.out.println("\nWell, " + name + ", I am thinking of a number between 1 and 20." + "\nTake a guess.");

            num = input.nextInt();

            while (num < 1 || num > 20) {
                if (num > 0 && num <= 20) {
                    break;
                }
                System.out.println("PLease enter a valid number between 1 to 20!");
                num = input.nextInt();

            }
            System.out.println("\033[0;1m" + num);


            int random = (int) (Math.random() * (20 - 1 + 1) + 1);

            while (num > 0 && num <= 20) {
                if (num > random) {
                    System.out.println("Your guess is too high.");
                    count++;

                } else if (num < random) {
                    System.out.println("Your guess is too low.");
                    count++;
                } else if (num == random) {
                    count++;
                    System.out.println("Good job, " + name + "! You guessed my number in " + count + " guesses!");
                    System.out.println("Would you like to play again? (y or n)");
                    break;
                }
                if (count >= 6) {
                    System.out.println("No luck! you try max guesses of 6 times ");
                    System.out.println("Would you like to play again? (y or n)");
                    break;
                }
                System.out.println("Take a Guss.");
                num = input.nextInt();
            }
            repeat = input.next();

            if (repeat.charAt(0) == 'y'){
                play = true;
                System.out.println("\033[0;1m" + repeat);
            }else {
                play = false;
                System.out.println("\033[0;1m" + repeat);
            }
        }while(play);

    }
}
